import tkinter as tk
import tkinter.messagebox as msg

from snake.game import SnakeGame
from snake.move_direction import MoveDirection


class MainForm(tk.Frame):
    BOARD_COLUMNS=10
    BOARD_ROWS=10

    def __init__(self, parent):
        tk.Frame.__init__(self, parent)
        self.pack(fill='both', expand=1)
        self.apple_image = tk.PhotoImage(file="resources/apple.gif")

        # create controls
        top_frame = tk.Frame(self)
        main_frame = tk.Frame(self)

        left_frame = tk.Frame(main_frame, bg="blue")
        right_frame = tk.Frame(main_frame, bg="yellow")

        but_start = tk.Button(top_frame, text="Start Training", command=self.start_training)
        but_stop = tk.Button(top_frame, text="Stop Training", command=self.stop_training)

        self.canvas = tk.Canvas(left_frame)
        label_top = tk.Frame(right_frame, width=200)
        self.label_info = tk.Label(right_frame)

        # lay controls to the window
        but_start.pack(side='left', padx=3, pady=3)
        but_stop.pack(side='left')

        top_frame.pack(fill=tk.X)
        main_frame.pack(fill=tk.BOTH, expand=tk.YES)

        right_frame.pack(side='right', fill=tk.Y)
        left_frame.pack(fill=tk.BOTH, expand=tk.YES)
        self.canvas.pack(fill=tk.BOTH, expand=tk.YES)
        label_top.pack(fill=tk.X)
        self.label_info.pack(fill=tk.BOTH, expand=tk.YES)
        self.game=SnakeGame(self.BOARD_COLUMNS, self.BOARD_ROWS)

    def on_key_press(self, event):
        if (event.keysym == "Down"):
            self.game.direction = MoveDirection.DOWN
            print("DOWN")
        if (event.keysym == "Up"):
            self.game.direction = MoveDirection.UP
            print("upppp")
        if (event.keysym == "Left"):
            self.game.direction = MoveDirection.LEFT
            print("leeeft")
        if (event.keysym == "Right"):
            self.game.direction = MoveDirection.RIGHT
            print("riiight")

    def on_window_shown(self):
        self.draw_game(self.game)

    def start_training(self):
        self.game = SnakeGame(self.BOARD_COLUMNS, self.BOARD_ROWS)
        self.draw_game(self.game)

    def stop_training(self):
        msg.showinfo(message="preved")
        self.label_info['text'] = "111"

    def on_tick(self):
        self.game.step()
        self.draw_game(self.game)
        if (not self.game.game_in_progress):
            self.label_info['text'] = "Game over!"

    def draw_game(self, game):
        w = self.canvas.winfo_width()
        h = self.canvas.winfo_height()
        row_w = w / self.BOARD_COLUMNS
        row_h = h / self.BOARD_ROWS
        self.canvas.delete("all")
        self.canvas.create_rectangle(0, 0, w, h, fill='black')
        # draw vertical lines
        for i in range(self.BOARD_COLUMNS):
            self.canvas.create_line(row_w * i, 0, row_w * i, h, dash=(4, 2), width=1, fill='green')
        # draw horizontal lines
        for i in range(self.BOARD_ROWS):
            self.canvas.create_line(0, row_h*i, w, row_h*i, dash=(4,2), width=1, fill='green')

        # draw apple
        img_w = self.apple_image.width()
        img_h = self.apple_image.height()


        for x in range(game.columns):
            for y in range(game.rows):
                x1 = row_w * x + 1
                y1 = row_h * y + 1
                x2 = row_w * (x + 1) - 1
                y2 = row_h * (y + 1) - 1
                if game.map[y, x] == SnakeGame.DATA_WORM:
                    self.canvas.create_rectangle(x1, y1, x2, y2, fill='blue')
                if game.map[y, x] == SnakeGame.DATA_APPLE:
                    #self.canvas.create_rectangle(x1, y1, x2, y2, fill='red')
                    self.canvas.create_image(x1 + img_w / 2, y1 + img_h / 2,
                                             anchor=tk.NW, image=self.apple_image)
