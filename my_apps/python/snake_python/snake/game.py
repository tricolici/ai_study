import numpy as np
import random
from snake.move_direction import MoveDirection


class SnakeGame:
    # Constants
    DATA_EMPTY=0
    DATA_WORM=1
    DATA_APPLE=5

    def __init__(self, columns, rows):
        self.columns = columns
        self.rows = rows
        self.game_in_progress = True
        self.map = np.full((rows,columns), self.DATA_EMPTY)
        self.head_x=1
        self.head_y=random.randint(0, self.rows-1)
        self.worm=[]
        self.worm.append((self.head_x - 1, self.head_y))
        self.worm.append((self.head_x, self.head_y))
        self.map[self.head_y][self.head_x-1] = self.DATA_WORM
        self.map[self.head_y][self.head_x] = self.DATA_WORM

        self.apple_x = 0
        self.apple_y = 0
        self.generate_apple()
        self.direction = MoveDirection.RIGHT
        self.apple_eaten=0

    def game_over(self):
        self.game_in_progress=False

    def generate_apple(self):
        empty_rows=[]
        for x in range(self.columns):
            for y in range(self.rows):
                if (self.map[y, x] == self.DATA_EMPTY):
                    empty_rows.append([x, y])
        if (len(empty_rows)==0):
            self.game_over()
            return
        idx=random.randint(0, len(empty_rows)-1)
        self.apple_x=empty_rows[idx][0]
        self.apple_y=empty_rows[idx][1]
        self.map[self.apple_y][self.apple_x] = self.DATA_APPLE

    def step(self):
        if (self.game_in_progress):
            self.move_snake()

    def move_snake(self):
        if (self.direction == MoveDirection.DOWN):
            if (self.head_y == self.rows - 1):
                self.game_over()
                return
            self.head_y+=1
            self.handleNextRow()
            return

        if (self.direction == MoveDirection.LEFT):
            if (self.head_x == 0):
                self.game_over()
                return
            self.head_x-=1
            self.handleNextRow()
            return

        if (self.direction == MoveDirection.RIGHT):
            if (self.head_x == self.columns - 1):
                self.game_over()
                return
            self.head_x+=1
            self.handleNextRow()
            return

        if (self.direction == MoveDirection.UP):
            if (self.head_y == 0):
                self.game_over()
                return
            self.head_y-=1
            self.handleNextRow()
            return
        raise Exception("WHaaat kind of direction is this?!?!")

    def handleNextRow(self):
        nextData = self.map[self.head_y, self.head_x]

        if (nextData == self.DATA_EMPTY):
            self.map[self.head_y, self.head_x] = self.DATA_WORM
            self.moveSnakeSegment()
            return
        if (nextData == self.DATA_WORM):
            self.game_over()
            return
        if (nextData == self.DATA_APPLE):
            self.map[self.head_y, self.head_x] = self.DATA_WORM
            self.worm.append([self.head_x, self.head_y])
            self.generate_apple()
            self.apple_eaten +=1


    def moveSnakeSegment(self):
        self.map[self.worm[0][1], self.worm[0][0]] = self.DATA_EMPTY
        self.worm.pop(0) # remove first element (oldest)
        self.worm.append((self.head_x, self.head_y))

