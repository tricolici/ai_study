from enum import Enum


class MoveDirection(Enum):
    UP = 1
    DOWN = 2
    RIGHT = 3
    LEFT = 4
