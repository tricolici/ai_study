import tkinter as tk
from main_form import MainForm

TICK_TIMER=400

def on_tick():
    main_form.on_tick()
    root.after(TICK_TIMER, on_tick)

def key_pressed(event):
    main_form.on_key_press(event)

if __name__ == "__main__" :
    formWidth = 800
    formHeight = 600
    root = tk.Tk()
    root.title('snake AI poc')
    root.geometry("%dx%d" % (formWidth, formHeight))
    root.resizable(False, False)
    main_form = MainForm(root)

    root.withdraw()
    root.update_idletasks()
    x = (root.winfo_screenwidth() - root.winfo_reqwidth()) / 2
    y = (root.winfo_screenheight() - root.winfo_reqheight()) / 2
    root.geometry("+%d+%d" % (x - formWidth/2, y - formHeight/2))
    root.deiconify()
    main_form.pack()
    root.update_idletasks()
    root.wait_visibility()
    main_form.on_window_shown()
    root.after(TICK_TIMER, on_tick)
    root.bind("<Key>", key_pressed)
    root.mainloop()

