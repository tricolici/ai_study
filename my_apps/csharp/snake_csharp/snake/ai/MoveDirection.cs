﻿namespace snake.ai
{
    public enum MoveDirection
    {
        Up,
        Down,
        Left,
        Right
    }
}
