﻿using System;
using System.Collections.Generic;

namespace snake.ai.algorithm.ga
{
	public class GA<T>
	{
		public List<DNA<T>> Population { get; private set; }
		public int Generation { get; private set; }

		public double BestFitness { get; private set; }

		public T[] BestGenes { get; private set; }

		public int Elitism;

		public double MutationRate;

		public Func<T> GetRandomGene { get; set; }

		public Func<int, double> FitnessFunction { get; set; }

		private List<DNA<T>> newPopulation;

		private int dnaSize;

		public GA(int populationSize, int dnaSize, int elitism, double mutationRate = 0.01f)
		{
			Generation = 1;
			Elitism = elitism;
			MutationRate = mutationRate;
			Population = new List<DNA<T>>(populationSize);
			newPopulation = new List<DNA<T>>(populationSize);
			this.dnaSize = dnaSize;

			BestGenes = new T[dnaSize];
		}

		public void initializePopulation()
		{
			for (int i = 0; i < Population.Capacity; i++)
			{

				Population.Add(createNewDNA(true));
			}
		}

		public void NewGeneration(int numNewDNA = 0, bool crossoverNewDNA = false)
		{
			int finalCount = Population.Count + numNewDNA;

			if (finalCount <= 0)
			{
				return;
			}

			if (Population.Count > 0)
			{
				CalculateFitness();
				Population.Sort(CompareDNA);
			}
			newPopulation.Clear();

			for (int i = 0; i < Population.Count; i++)
			{
				if (i < Elitism && i < Population.Count)
				{
					newPopulation.Add(Population[i]);
				}
				else if (i < Population.Count || crossoverNewDNA)
				{
					DNA<T> parent1 = ChooseParent();
					DNA<T> parent2 = ChooseParent();

					DNA<T> child = parent1.Crossover(parent2);

					child.Mutate(MutationRate);

					newPopulation.Add(child);
				}
				else
				{
					newPopulation.Add(createNewDNA(true));
				}
			}

			List<DNA<T>> tmpList = Population;
			Population = newPopulation;
			newPopulation = tmpList;

			Generation++;
		}

		private int CompareDNA(DNA<T> a, DNA<T> b)
		{
			if (a.Fitness > b.Fitness)
			{
				return -1;
			}
			else if (a.Fitness < b.Fitness)
			{
				return 1;
			}
			else
			{
				return 0;
			}
		}

		private void CalculateFitness()
		{
			DNA<T> best = Population[0];

			for (int i = 0; i < Population.Count; i++)
			{
				Population[i].CalculateFitness(i);

				if (Population[i].Fitness > best.Fitness)
				{
					best = Population[i];
				}
			}

			BestFitness = best.Fitness;
			best.Genes.CopyTo(BestGenes, 0);
		}

		private DNA<T> ChooseParent()
		{
			if (Population.Count > 0)
			{
				int idx = Utils.randomInt(0, Population.Count);
				return Population[idx];
			}

			return null;
		}

		private DNA<T> createNewDNA(bool shouldRandomizeGenes)
		{
			var dna = new DNA<T>(dnaSize);
			dna.FitnessFunction = this.FitnessFunction;
			dna.GetRandomGene = this.GetRandomGene;

			if (shouldRandomizeGenes)
			{
				dna.randomizeGenes();
			}

			return dna;
		}
	}
}
