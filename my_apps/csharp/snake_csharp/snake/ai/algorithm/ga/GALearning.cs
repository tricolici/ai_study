﻿using AForge.Neuro;
using System;
using System.Text;

namespace snake.ai.algorithm.ga
{
    public class GALearning : GenericAlgorithm
    {
        public const int inputsCount = 8;
        public const int outputsCount = 4;

        private ActivationNetwork network;
        private GA<double> ga;

        public GALearning()
        {
            this.trainingInProgress = false;
            this.shouldStopTraining = false;

            network = new ActivationNetwork(new BipolarSigmoidFunction(GameParameters.GA_SIGMOID_ALPHA),
                inputsCount, GameParameters.GA_HIDDEN_LAYER_NEURONS, outputsCount);

            ga = new GA<double>(GameParameters.GA_POPULATION_SIZE, calculateNetworkSize(),
                GameParameters.GA_ELITISM, GameParameters.GA_MUTATION_RATE);
            ga.FitnessFunction = FitnessFunction;
            ga.GetRandomGene = getRandomDouble;
        }

        public override SnakeGame createDemoGame()
        {
            return new GASnakeGame();
        }

        public override void train()
        {
            trainingInProgress = true;
            shouldStopTraining = false;
            ga.initializePopulation(); //TODO: Load best network from previous trainings

            int it = 1;

            while (ga.BestFitness < GameParameters.GA_FITNESS_STOP)
            {
                ga.NewGeneration();
                if (shouldStopTraining)
                    break;

                if (it % 10 == 0)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("Gen: " + it.ToString() + "\n");
                    sb.Append(String.Format("BstFtn: {0:0.00}%", ga.BestFitness));
                    UpdateLogs(sb.ToString());
                }

                it++;
            }

            //Save best genes to then network for a demo play!
            setNetworkWeights(ga.BestGenes);
            trainingInProgress = false;
        }

        public override MoveDirection calculateNextMove(SnakeGame game)
        {
            double[] input = (game as GASnakeGame).getGameState();
            double[] output = network.Compute(input);
            return (game as GASnakeGame).translateNextMoveFromNN(output);
        }

        public override void SaveWeights(string fileName)
        {
            throw new NotImplementedException();
        }

        public override void LoadWeights(string fileName)
        {
            throw new NotImplementedException();
        }

        private int calculateNetworkSize()
        {
            int result = 0;

            for (int l=0; l<network.Layers.Length; l++)
            {
                for (int n = 0; n < network.Layers[l].Neurons.Length; n++)
                {
                    var neuron = network.Layers[l].Neurons[0];
                    result += neuron.Weights.Length;
                    if (neuron is ActivationNeuron)
                    {
                        result += 1; // for Threshold
                    } else
                    {
                        throw new Exception("WHAAT. Invalid neuron type? " + neuron.GetType().Name);
                    }
                }
            }

            return result;
        }

        private double getRandomDouble()
        {
            return Utils.randomDouble();
        }

        private double FitnessFunction(int index)
        {
            DNA<double> dna = ga.Population[index];
            setNetworkWeights(dna.Genes);

            double apples = 0;

            for (int i=0; i<GameParameters.GA_GAMES_TO_PLAY_FOR_FITNESS_CALCULATION; i++)
            {
                GASnakeGame game = (GASnakeGame)createDemoGame();
                while (game.isGameInProgress() && (game.Turns < GameParameters.GA_MAX_MOVES_TO_PLAY_PER_GAME))
                {
                    MoveDirection nextMove = calculateNextMove(game);
                    game.setDirection(nextMove);
                    game.NextTurn();
                }

                apples += game.getAppleEaten();
            }

            return apples / GameParameters.GA_GAMES_TO_PLAY_FOR_FITNESS_CALCULATION;
        }

        private void setNetworkWeights(double[] input)
        {
            int idx = 0;

            foreach(var layer in network.Layers)
            {
                foreach(var neuron in layer.Neurons)
                {
                    if (neuron is ActivationNeuron)
                    {
                        (neuron as ActivationNeuron).Threshold = input[idx++];

                        for (int i=0; i<neuron.Weights.Length; i++)
                        {
                            neuron.Weights[i] = input[idx++];
                        }
                    }
                    else
                    {
                        throw new Exception("WHAAT. Invalid neuron type? " + neuron.GetType().Name);
                    }
                }
            }
        }
    }
}
