﻿
namespace snake.ai.algorithm.ga
{
    public class GASnakeGame : SnakeGame
    {
        public GASnakeGame()
        {
        }

        protected override void onAppleEat()
        {
        }

        protected override void onGameOver()
        {
        }

        protected override void onMoveToAppleDirection()
        {
        }

        protected override void onNextTurn()
        {
        }

        public double[] getGameState()
        {
            double[] state = new double[GALearning.inputsCount];

            // move LEFT is blocked
            state[0] = bool2double((headX == 0) || (map[headX - 1, headY] == DATA_WORM));
            // move RIGHT is blocked
            state[1] = bool2double((headX == SizeX - 1) || (map[headX + 1, headY] == DATA_WORM));
            // move UP is blocked
            state[2] = bool2double((headY == 0) || (map[headX, headY - 1] == DATA_WORM));
            // move DOWN is blocked
            state[3] = bool2double((headY == SizeY - 1) || (map[headX, headY + 1] == DATA_WORM));

            // is FOOD on the left
            state[4] = bool2double(appleX < headX);
            // is FOOD on the right
            state[5] = bool2double(appleX > headX);
            // is FOOD up
            state[6] = bool2double(appleY < headY);
            // is FOOD down
            state[7] = bool2double(appleY > headY);

            return state;
        }

        public MoveDirection translateNextMoveFromNN(double[] aiDecision)
        {
            int max_index = 0;

            for (int i=0; i<aiDecision.Length; i++)
            {
                if (aiDecision[i] > aiDecision[max_index])
                {
                    max_index = i;
                }
            }

            switch (max_index)
            {
                case 0: return MoveDirection.Left;
                case 1: return MoveDirection.Right;
                case 2: return MoveDirection.Up;
            }

            return MoveDirection.Down;
        }

        private double bool2double(bool value)
        {
            return value ? 1.0 : 0.0;
        }
    }
}
