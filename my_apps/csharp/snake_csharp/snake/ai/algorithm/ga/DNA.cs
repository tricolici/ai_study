﻿using System;

namespace snake.ai.algorithm.ga
{
    public class DNA<T>
    {
        public T[] Genes { get; private set; }
        public double Fitness { get; private set; }

        public Func<T> GetRandomGene { get; set; }

        public Func<int, double> FitnessFunction { get; set; }

        public DNA(int size)
        {
            Genes = new T[size];
        }

        public void randomizeGenes()
        {
            for (int i = 0; i < Genes.Length; i++)
            {
                Genes[i] = GetRandomGene();
            }
        }

        public void CalculateFitness(int index)
        {
            this.Fitness = FitnessFunction(index);
        }

        public DNA<T> Crossover(DNA<T> otherParent)
        {
            DNA<T> child = new DNA<T>(Genes.Length);
            child.FitnessFunction = FitnessFunction;
            child.GetRandomGene = GetRandomGene;

            for (int i = 0; i < Genes.Length; i++)
            {
                child.Genes[i] = Utils.randomDouble() < 0.5 ? Genes[i] : otherParent.Genes[i];
            }

            return child;
        }

        public void Mutate(double mutationRate)
        {
            for (int i = 0; i < Genes.Length; i++)
            {
                if (Utils.randomDouble() < mutationRate)
                {
                    Genes[i] = GetRandomGene();
                }
            }
        }
    }
}
