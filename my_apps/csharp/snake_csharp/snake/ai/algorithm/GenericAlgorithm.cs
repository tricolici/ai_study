﻿using snake.ai.algorithm.ga;
using snake.ai.algorithm.qlearning;

namespace snake.ai.algorithm
{
    public abstract class GenericAlgorithm
    {

        #region Private Data
        protected bool trainingInProgress = false;
        protected bool shouldStopTraining = false;
        #endregion

        public abstract void train();
        

        public abstract MoveDirection calculateNextMove(SnakeGame game);


        public abstract void SaveWeights(string fileName);

        public abstract void LoadWeights(string fileName);

        public abstract SnakeGame createDemoGame();

        public void stopTraining()
        {
            this.shouldStopTraining = true;
        }

        public bool isTrainingInProgress()
        {
            return trainingInProgress;
        }

        public MainForm mainForm { get; set; }

        public void UpdateLogs(string data)
        {
            MainForm.UpdateLogs(mainForm, data);
        }

        public static GenericAlgorithm create(string algoritghm)
        {
            if (algoritghm == "Q-Learning")
            {
                return new QLearning();
            }

            if (algoritghm == "Genetic Algorithm")
            {
                return new GALearning();
            }

            return null;
        }
    }
}
