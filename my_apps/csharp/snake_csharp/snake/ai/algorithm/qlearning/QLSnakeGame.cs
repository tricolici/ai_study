﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace snake.ai.algorithm.qlearning
{
    public class QLSnakeGame : SnakeGame
    {
        public const int INPUT_STATE_BITS = 8;
        public const double REWARD_DIE = -10.0;
        public const double REWARD_APPLE = 10.0;
        public const double REWARD_MOVE_TO_APPLE = 1.0;

        public double Reward { get; private set; }

        protected override void onAppleEat()
        {
            this.Reward = REWARD_APPLE;
        }

        protected override void onGameOver()
        {
            this.Reward = REWARD_DIE;
        }

        protected override void onMoveToAppleDirection()
        {
            this.Reward = REWARD_MOVE_TO_APPLE;
        }

        protected override void onNextTurn()
        {
            this.Reward = 0.0;
        }

        public static int getMaximumStates()
        {
            return (int)Math.Pow(2, INPUT_STATE_BITS);
        }

        public int calculateGameState()
        {
            bool[] bits = new bool[INPUT_STATE_BITS];

            // is move LEFT is blocked
            bits[0] = (headX == 0) || (map[headX - 1, headY] == DATA_WORM);
            // is move RIGHT is blocked
            bits[1] = (headX == SizeX - 1) || (map[headX + 1, headY] == DATA_WORM);
            // is move UP is blocked
            bits[2] = (headY == 0) || (map[headX, headY - 1] == DATA_WORM);
            // is move DOWN is blocked
            bits[3] = (headY == SizeY - 1) || (map[headX, headY + 1] == DATA_WORM);

            // is FOOD on the left
            bits[4] = appleX < headX;
            // is FOOD on the right
            bits[5] = appleX > headX;
            // is FOOD up
            bits[6] = appleY < headY;
            // is FOOD down
            bits[7] = appleY > headY;

            return Utils.boolsToIntAsBits(bits);
        }
    }
}
