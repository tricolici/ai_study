﻿using System;
using System.IO;
using System.Text;

namespace snake.ai.algorithm.qlearning
{
    public class QLearning : GenericAlgorithm
    {
        #region Private Data
        private int numberOfPossibleActions;
        private int numberOfPossibleStates;
        private double[,] brain = null;
        #endregion

        public QLearning()
        {
            this.shouldStopTraining = false;
            this.trainingInProgress = false;
            this.numberOfPossibleStates = GameParameters.QLEARNING_MAX_STATES;
            this.numberOfPossibleActions = GameParameters.QLEARNING_MAX_ACTIONS;

            this.brain = new double[numberOfPossibleStates, numberOfPossibleActions];
        }

        public override MoveDirection calculateNextMove(SnakeGame game)
        {
            if (game is QLSnakeGame)
            {
                int gameState = (game as QLSnakeGame).calculateGameState();
                int action = getBrainMaxValueIndex(gameState);
                return convertActionToDecision(action);
            }
            throw new Exception("Bad SnakeClass instance sent to QLearning ;(");
        }

        public override void train()
        {
            shouldStopTraining = false;
            trainingInProgress = true;

            int iterationsCount = GameParameters.QLEARNING_TRAIN_ITERATIONS;
            int maxMovesPerGame = GameParameters.QLEARNING_ITERATION_LIMIT_MOVES;

            for (int it = 0; it<iterationsCount; it++)
            {
                double percent = (100.0 / (double)iterationsCount) * (double)it;
                double epsilon = getEpsilon(percent);

                training_iteration(it, maxMovesPerGame, epsilon);
                if (it % 500 == 0)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append("Iteration: " + it.ToString() + "\n");
                    sb.Append("Remaining: " + (iterationsCount - it).ToString() + "\n");
                    sb.Append(String.Format("Progress: {0:0.00}%", percent));
                    UpdateLogs(sb.ToString());
                }

                if (shouldStopTraining)
                    break;
            }

            UpdateLogs("Training finished");
            trainingInProgress = false;
        }

        private double getEpsilon(double iterationsProgress)
        {
            if (iterationsProgress < 5)
            {
                return 1; // exporation 100%
            }

            if (iterationsProgress < 20)
            {
                return 0.5;
            }

            if (iterationsProgress < 90)
            {
                return 0.3;
            }

            return 0.1;
        }

        private void training_iteration(int iterationIndex, int maxMovesPerGame, double epsilon)
        {
            QLSnakeGame game = new QLSnakeGame();
            int gameState = game.calculateGameState();
            double gamma = GameParameters.QLEARNING_GAMMA;

            while (game.isGameInProgress())
            {
                int action = 0;

                if (Utils.randomDouble() < epsilon)
                {
                    action = Utils.randomInt(0, numberOfPossibleActions);
                } else
                {
                    action = getBrainMaxValueIndex(gameState);
                }
                game.setDirection(convertActionToDecision(action));
                game.NextTurn();
                int newState = game.calculateGameState();

                this.brain[gameState, action] = game.Reward + gamma * getBrainMaxValue(newState);

                gameState = newState;

                if ((maxMovesPerGame > 0) && (game.Turns >= maxMovesPerGame))
                {
                    break;// enough moves baby
                }
            }
        }

        private double getBrainMaxValue(int gameState)
        {
            double max = double.MinValue;

            for (int i = 0; i < numberOfPossibleActions; i++)
            {
                max = Math.Max(max, this.brain[gameState, i]);
            }

            return max;
        }

        private int getBrainMaxValueIndex(int gameState)
        {
            int idx = -1;
            double max = double.MinValue;

            for (int i = 0; i < numberOfPossibleActions; i++)
            {
                if (max < this.brain[gameState, i])
                {
                    max = this.brain[gameState, i];
                    idx = i;
                }
            }
            
            return idx;
        }

        public override void SaveWeights(string fileName)
        {
            // new double[numberOfPossibleStates, numberOfActions];
            using (StreamWriter stream = new StreamWriter(fileName))
            {
                StringBuilder sb = new StringBuilder();

                for (int i=0; i<numberOfPossibleStates; i++)
                {
                    for (int j=0; j<numberOfPossibleActions; j++)
                    {
                        sb.Append(this.brain[i, j]);
                        if (j < numberOfPossibleActions - 1)
                        {
                            sb.Append(GameParameters.CSV_SEPARATOR);
                        }
                    }
                    sb.AppendLine();
                }

                stream.Write(sb.ToString());
                stream.Flush();
            }
        }

        public override void LoadWeights(string fileName)
        {
            using (StreamReader stream = new StreamReader(fileName))
            {
                for (int i = 0; i < numberOfPossibleStates; i++)
                {
                    string[] str = stream.ReadLine().Split(GameParameters.CSV_SEPARATOR);
                    for (int j=0; j < numberOfPossibleActions; j++)
                    {
                        this.brain[i, j] = double.Parse(str[j].Trim());
                    }
                }
            }
        }

        private string getBrainAsString()
        {
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < numberOfPossibleStates; i++)
            {
                for (int j = 0; j < numberOfPossibleActions; j++)
                {
                    sb.Append(this.brain[i, j]);
                    if (j < numberOfPossibleActions - 1)
                    {
                        sb.Append(GameParameters.CSV_SEPARATOR);
                    }
                }
                sb.AppendLine();
            }

            return sb.ToString();
        }

        public string getBrainMD5()
        {
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = Encoding.ASCII.GetBytes(getBrainAsString());
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }

                return sb.ToString();
            }
        }

        public override SnakeGame createDemoGame()
        {
            return new QLSnakeGame();
        }

        private MoveDirection convertActionToDecision(int action)
        {
            switch (action)
            {
                case 0: return MoveDirection.Down;
                case 1: return MoveDirection.Up;
                case 2: return MoveDirection.Left;
                case 3: return MoveDirection.Right;
            }

            throw new Exception("INVALID ACTION. WHAAT is going on");
        }
    }
}
