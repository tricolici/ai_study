﻿using snake.ai;
using snake.ai.algorithm.qlearning;
using System;

namespace snake
{
    static class GameParameters
    {
        // Generic Game Constants
        public static int BOARD_WIDTH = 30;
        public static int BOARD_HEIGHT = 30;
        public const char CSV_SEPARATOR = ';'; 

        // Q Learning constants
        public static readonly int QLEARNING_MAX_STATES = QLSnakeGame.getMaximumStates();
        public static readonly int QLEARNING_MAX_ACTIONS = Enum.GetNames(typeof(MoveDirection)).Length;
        public const double QLEARNING_GAMMA = 0.8; //Discount Rate

        // Q Learning settings (can be modified)
        public static int QLEARNING_TRAIN_ITERATIONS = 500000;
        // this is needed to stop snakes that are moving in a loop without ending the game or eating the apple
        public static int QLEARNING_ITERATION_LIMIT_MOVES = 500;

        // Generic Algorithm Settings
        public static double GA_SIGMOID_ALPHA = 2.0;
        public static int GA_HIDDEN_LAYER_NEURONS = 50;
        public static int GA_POPULATION_SIZE = 50;
        public static double GA_MUTATION_RATE = 0.02;
        public static int GA_ELITISM = 5;
        public static double GA_FITNESS_STOP = 100;
        public static int GA_GAMES_TO_PLAY_FOR_FITNESS_CALCULATION = 10;
        public static int GA_MAX_MOVES_TO_PLAY_PER_GAME = 300;
    }
}
