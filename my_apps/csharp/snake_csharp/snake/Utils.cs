﻿using System;
using System.Collections;
using System.Drawing;
using System.Windows.Forms;

namespace snake
{
    public class Utils
    {
        public const int RAW_IMAGE_SIZE = 20;

        private static Random rnd = new Random();
        public static int randomInt(int min, int max)
        {
            return rnd.Next(min, max);
        }

        public static double randomDouble()
        {
            return rnd.NextDouble();
        }

        public static int boolsToIntAsBits(bool[] bits)
        {
            int[] array = new int[1];
            new BitArray(bits).CopyTo(array, 0);
            return array[0];
        }

        public static Bitmap getAsImage(SnakeGame game)
        {
            
            Bitmap bmp = new Bitmap(game.SizeX * RAW_IMAGE_SIZE, game.SizeY * RAW_IMAGE_SIZE);

            int maxX = game.SizeX * RAW_IMAGE_SIZE - 1;
            int maxY = game.SizeY * RAW_IMAGE_SIZE - 1;

            using (Graphics g = Graphics.FromImage(bmp))
            {
                g.FillRectangle(Brushes.White, new Rectangle(0, 0, game.SizeX * RAW_IMAGE_SIZE, game.SizeY * RAW_IMAGE_SIZE - 1));

                // draw vertical lines
                for (int i = 0; i < game.SizeX; i++)
                {
                    int x = i * RAW_IMAGE_SIZE;
                    g.DrawLine(Pens.Gray, x, 0, x, maxY);
                }
                g.DrawLine(Pens.Gray, maxX, 0, maxX, maxY);

                // draw horizontal lines
                for (int i = 0; i < game.SizeY; i++)
                {
                    int y = i * RAW_IMAGE_SIZE;
                    g.DrawLine(Pens.Gray, 0, y, maxX, y);
                }
                g.DrawLine(Pens.Gray, 0, maxY, maxX, maxY);

                // Draw Apple
                g.FillRectangle(Brushes.Red, game.appleX * RAW_IMAGE_SIZE + 1, game.appleY * RAW_IMAGE_SIZE + 1,
                    RAW_IMAGE_SIZE - 1, RAW_IMAGE_SIZE - 1);

                SolidBrush headBrush = new SolidBrush(Color.FromArgb(0, 0, 102));
                SolidBrush snakeBrush = new SolidBrush(Color.FromArgb(0, 80, 176));


                for (int i=0; i< game.worm.Count; i++)
                {
                    Brush brush = (i >= game.worm.Count - 1) ? headBrush : snakeBrush;
                    int x1 = game.worm[i].x * RAW_IMAGE_SIZE + 1;
                    int y1 = game.worm[i].y * RAW_IMAGE_SIZE + 1;
                    g.FillRectangle(brush, x1, y1, RAW_IMAGE_SIZE - 1, RAW_IMAGE_SIZE - 1);
                }
            }

            return bmp;
        }
    }
}
