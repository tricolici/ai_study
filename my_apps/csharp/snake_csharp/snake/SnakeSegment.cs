﻿namespace snake
{
    public class SnakeSegment
    {
        public int x;
        public int y;

        public SnakeSegment(int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }
}
