﻿using snake.ai;
using snake.ai.algorithm;
using snake.ai.algorithm.qlearning;
using System;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace snake
{
    public partial class MainForm : Form
    {
        private SnakeGame demo = null;
        private GenericAlgorithm teacher = null;
        private bool demoGameInProgress = false;

        public MainForm()
        {
            InitializeComponent();
            txtLogs.Text = "";
            txtBoadWidth.Value = GameParameters.BOARD_WIDTH;
            txtBoadHeight.Value = GameParameters.BOARD_HEIGHT;
            txtQLearningIterations.Value = GameParameters.QLEARNING_TRAIN_ITERATIONS / 1000;
            txtQLearningItMaxMoves.Value = GameParameters.QLEARNING_ITERATION_LIMIT_MOVES;
            ctrlAlgorithm.SelectedIndex = 1;
        }

        private void drawGame()
        {
            if (demo != null)
            {
                pictureBox1.Image = Utils.getAsImage(demo);
            }
        }

        private void butStartDemoGame_Click(object sender, EventArgs e)
        {
            if (this.teacher == null)
            {
                this.demoGameInProgress = false;
                MessageBox.Show("Invoke training !");
                return;
            }
            demo = teacher.createDemoGame();
            demo.LimitNumberOfMoves = false;
            drawGame();
            txtLogs.Text = "";
            this.demoGameInProgress = true;
            timer1.Interval = 10 + (1000 - trackGameSpeed.Value*100);
            timer1.Enabled = true;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (demoGameInProgress)
            {
                var decision = this.teacher.calculateNextMove(demo);
                demo.setDirection(decision);
                demo.NextTurn();
                drawGame();

                if (!demo.isGameInProgress())
                {
                    timer1.Enabled = false;
                    StringBuilder sb = new StringBuilder();
                    sb.Append("Game Over!\n");
                    sb.Append("Apples collected:");
                    sb.Append(demo.getAppleEaten());
                    txtLogs.Text = sb.ToString();
                    timer1.Enabled = false;
                    demoGameInProgress = false;
                }
                else
                {
                    timer1.Interval = 10 + (1000 - trackGameSpeed.Value * 100);
                }
                return;
            }
        }

        private void enableControls(bool enable)
        {
            butStop.Enabled = !enable;
            butStartGame.Enabled = enable;
            butTrainAI.Enabled = enable;
        }

        public static void UpdateLogs(Form form, string message)
        {
            MainForm mform = (MainForm)form;
            if (mform.txtLogs.InvokeRequired)
            {
                mform.Invoke(new Action(() => mform.txtLogs.Text = message));
            } else
            {
                mform.txtLogs.Text = message;
            }
        }

        private async void butTrainAI_Click(object sender, EventArgs e)
        {
            enableControls(false);
            string algorithmName = ctrlAlgorithm.Text;
            await Task.Run(() => trainNetworks(algorithmName));
            enableControls(true);
        }

        private void trainNetworks(string algorithmName)
        {
            this.teacher = GenericAlgorithm.create(algorithmName);
            teacher.mainForm = this;
            this.teacher.train();
        }

        private void butStop_Click(object sender, EventArgs e)
        {
            if (teacher != null)
            {
                teacher.stopTraining();
            }
        }

        private void butSave_Click(object sender, EventArgs e)
        {
            if (teacher == null)
            {
                MessageBox.Show("brain is empty. what to save? Invoke a training please");
                return;
            }
            DialogResult dlgResult = saveFileDlg.ShowDialog(this);
            if (dlgResult == DialogResult.OK)
            {
                teacher.SaveWeights(saveFileDlg.FileName);
            }
        }

        private void butLoad_Click(object sender, EventArgs e)
        {
            if ((teacher != null) && teacher.isTrainingInProgress())
            {
                MessageBox.Show("Cannot load while training!");
                return;
            }

            DialogResult dlgResult = openFileDlg.ShowDialog(this);
            if (dlgResult == DialogResult.OK)
            {
                teacher = new QLearning();
                teacher.LoadWeights(openFileDlg.FileName);
            }
        }

        private void txtBoadWidth_ValueChanged(object sender, EventArgs e)
        {
            GameParameters.BOARD_WIDTH = Convert.ToInt32(txtBoadWidth.Value);
        }

        private void txtBoadHeight_ValueChanged(object sender, EventArgs e)
        {
            GameParameters.BOARD_HEIGHT = Convert.ToInt32(txtBoadHeight.Value);
        }

        private void txtQLearningIterations_ValueChanged(object sender, EventArgs e)
        {
            GameParameters.QLEARNING_TRAIN_ITERATIONS = Convert.ToInt32(txtQLearningIterations.Value * 1000);
        }

        private void txtQLearningItMaxMoves_ValueChanged(object sender, EventArgs e)
        {
            GameParameters.QLEARNING_ITERATION_LIMIT_MOVES = Convert.ToInt32(txtQLearningItMaxMoves.Value);
        }
    }
}
