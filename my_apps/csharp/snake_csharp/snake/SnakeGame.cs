﻿using snake.ai;
using System;
using System.Collections.Generic;

namespace snake
{
    public abstract class SnakeGame
    {
        public const int MOVES_LEFT = 100;

        public const byte DATA_EMPTY = 0;
        public const byte DATA_WORM = 1;
        public const byte DATA_APPLE = 2;

        // Game Data
        protected byte[,] map;
        private MoveDirection direction;
        private int appleEaten = 0;
        private int movesLeft = MOVES_LEFT;

        // Additional variables
        public bool LimitNumberOfMoves { get; set; } = false;

        public int Turns { get; private set; }

        public List<SnakeSegment> worm { get; private set; }
        private bool gameInProgress;
        protected int headX;
        protected int headY;
        public int appleX { get; private set; }
        public int appleY { get; private set; }

        public int SizeX { get; private set; }
        public int SizeY { get; private set; }

        protected abstract void onNextTurn();
        protected abstract void onGameOver();
        protected abstract void onAppleEat();
        protected abstract void onMoveToAppleDirection();

        public SnakeGame()
        {
            this.gameInProgress = true;
            this.SizeX = GameParameters.BOARD_WIDTH;
            this.SizeY = GameParameters.BOARD_HEIGHT;

            this.map = new byte[SizeX, SizeY];

            for (int x=0; x<SizeX; x++)
            {
                for (int y=0; y<SizeY; y++)
                {
                    map[x, y] = DATA_EMPTY;
                }
            }

            this.headX = 1;
            this.headY = Utils.randomInt(0, SizeY);
            this.worm = new List<SnakeSegment>();

            worm.Add(new SnakeSegment(headX - 1, headY));
            worm.Add(new SnakeSegment(headX, headY));
            map[headX - 1, headY] = DATA_WORM;
            map[headX, headY] = DATA_WORM;

            this.direction = MoveDirection.Right;

            generateApple();
        }

        public void NextTurn()
        {
            if (gameInProgress)
            {
                onNextTurn();
                moveSnake();
                Turns++;
            }
        }

        public MoveDirection getDirection()
        {
            return this.direction;
        }

        public void setDirection(MoveDirection direction)
        {
            if (gameInProgress)
            {
                this.direction = direction;
            }
        }

        private void gameOver()
        {
            this.gameInProgress = false;
            onGameOver();
        }

        private void generateApple()
        {
            List<SnakeSegment> emptyRows = new List<SnakeSegment>();
            for (int x = 0; x<SizeX; x++ )
            {
                for (int y = 0; y< SizeY; y++ )
                {
                    if (map[x, y] == DATA_EMPTY)
                    {
                        emptyRows.Add(new SnakeSegment(x, y));
                    }
                }
            }

            if (emptyRows.Count == 0)
            {
                gameOver();
                return;
            }

            int idx = Utils.randomInt(0, emptyRows.Count);
            this.appleX = emptyRows[idx].x;
            this.appleY = emptyRows[idx].y;
            map[appleX, appleY] = DATA_APPLE;
        }

        private void moveSnakeSegment()
        {
            map[worm[0].x, worm[0].y] = DATA_EMPTY;

            for (int i = 0; i < worm.Count - 1; i++)
            {
                worm[i].x = worm[i + 1].x;
                worm[i].y = worm[i + 1].y;
            }

            worm[worm.Count - 1].x = headX;
            worm[worm.Count - 1].y = headY;
        }

        private void handleNextRow()
        {
            double nextData = map[headX, headY];

            if (nextData == DATA_EMPTY)
            {
                // just move the snake
                map[headX, headY] = DATA_WORM;
                moveSnakeSegment();

                if (LimitNumberOfMoves)
                {
                    movesLeft--;
                    if (movesLeft <= 0)
                    {
                        gameOver();
                    }
                }
                return;
            }

            if (nextData == DATA_WORM)
            {
                gameOver();
                return;
            }

            if (nextData == DATA_APPLE)
            {
                // eat the apple.
                map[headX, headY] = DATA_WORM;
                worm.Add(new SnakeSegment(headX, headY));
                generateApple();
                movesLeft = MOVES_LEFT;
                appleEaten++;
                onAppleEat();
            }
        }

        private void moveSnake()
        {
            if (direction == MoveDirection.Down)
            {
                if (headY == SizeY - 1)
                {
                    
                    gameOver();
                    return;
                }
                if (headY < appleY)
                {
                    onMoveToAppleDirection();
                }

                headY++;
                handleNextRow();
                return;
            }

            if (direction == MoveDirection.Left)
            {
                if (headX == 0)
                {
                    gameOver();
                    return;
                }
                if (appleX < headX)
                {
                    onMoveToAppleDirection();
                }

                headX--;
                handleNextRow();
                return;
            }

            if (direction == MoveDirection.Right)
            {
                if (headX == SizeX - 1)
                {
                    gameOver();
                    return;
                }
                if (appleX > headX)
                {
                    onMoveToAppleDirection();
                }
                headX++;
                handleNextRow();
                return;
            }

            if (direction == MoveDirection.Up)
            {
                if (headY == 0)
                {
                    gameOver();
                    return;
                }
                if (appleY < headY)
                {
                    onMoveToAppleDirection();
                }
                headY--;
                handleNextRow();
                return;
            }

            throw new Exception(String.Format("WTF. Direction is {0}", direction));
        }

        public int getLength()
        {
            return this.worm.Count;
        }

        public bool isGameInProgress()
        {
            return gameInProgress;
        }

        public int getAppleEaten()
        {
            return this.appleEaten;
        }

        public bool isApple(int x, int y)
        {
            return this.map[x, y] == DATA_APPLE;
        }

        public bool isWorm(int x, int y)
        {
            return this.map[x, y] == DATA_WORM;
        }

        public bool isEmpty(int x, int y)
        {
            return this.map[x, y] == DATA_EMPTY;
        }
    }
}
