﻿
namespace snake
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.butStartGame = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.butTrainAI = new System.Windows.Forms.Button();
            this.butStop = new System.Windows.Forms.Button();
            this.txtLogs = new System.Windows.Forms.Label();
            this.butSave = new System.Windows.Forms.Button();
            this.butLoad = new System.Windows.Forms.Button();
            this.saveFileDlg = new System.Windows.Forms.SaveFileDialog();
            this.openFileDlg = new System.Windows.Forms.OpenFileDialog();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabGame = new System.Windows.Forms.TabPage();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tabSettings = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtQLearningItMaxMoves = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.txtQLearningIterations = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.trackGameSpeed = new System.Windows.Forms.TrackBar();
            this.label6 = new System.Windows.Forms.Label();
            this.txtBoadHeight = new System.Windows.Forms.NumericUpDown();
            this.txtBoadWidth = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.ctrlAlgorithm = new System.Windows.Forms.ComboBox();
            this.tabControl1.SuspendLayout();
            this.tabGame.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.tabSettings.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtQLearningItMaxMoves)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQLearningIterations)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackGameSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBoadHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBoadWidth)).BeginInit();
            this.SuspendLayout();
            // 
            // butStartGame
            // 
            this.butStartGame.Location = new System.Drawing.Point(230, 12);
            this.butStartGame.Name = "butStartGame";
            this.butStartGame.Size = new System.Drawing.Size(124, 23);
            this.butStartGame.TabIndex = 1;
            this.butStartGame.TabStop = false;
            this.butStartGame.Text = "Demo Game";
            this.butStartGame.UseVisualStyleBackColor = true;
            this.butStartGame.Click += new System.EventHandler(this.butStartDemoGame_Click);
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // butTrainAI
            // 
            this.butTrainAI.Location = new System.Drawing.Point(12, 12);
            this.butTrainAI.Name = "butTrainAI";
            this.butTrainAI.Size = new System.Drawing.Size(105, 23);
            this.butTrainAI.TabIndex = 4;
            this.butTrainAI.TabStop = false;
            this.butTrainAI.Text = "Train AI";
            this.butTrainAI.UseVisualStyleBackColor = true;
            this.butTrainAI.Click += new System.EventHandler(this.butTrainAI_Click);
            // 
            // butStop
            // 
            this.butStop.Enabled = false;
            this.butStop.Location = new System.Drawing.Point(123, 12);
            this.butStop.Name = "butStop";
            this.butStop.Size = new System.Drawing.Size(101, 23);
            this.butStop.TabIndex = 8;
            this.butStop.TabStop = false;
            this.butStop.Text = "Stop Training";
            this.butStop.UseVisualStyleBackColor = true;
            this.butStop.Click += new System.EventHandler(this.butStop_Click);
            // 
            // txtLogs
            // 
            this.txtLogs.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLogs.AutoSize = true;
            this.txtLogs.Location = new System.Drawing.Point(605, 68);
            this.txtLogs.Name = "txtLogs";
            this.txtLogs.Size = new System.Drawing.Size(35, 13);
            this.txtLogs.TabIndex = 9;
            this.txtLogs.Text = "label1";
            // 
            // butSave
            // 
            this.butSave.Location = new System.Drawing.Point(360, 12);
            this.butSave.Name = "butSave";
            this.butSave.Size = new System.Drawing.Size(104, 23);
            this.butSave.TabIndex = 10;
            this.butSave.TabStop = false;
            this.butSave.Text = "Save";
            this.butSave.UseVisualStyleBackColor = true;
            this.butSave.Click += new System.EventHandler(this.butSave_Click);
            // 
            // butLoad
            // 
            this.butLoad.Location = new System.Drawing.Point(470, 12);
            this.butLoad.Name = "butLoad";
            this.butLoad.Size = new System.Drawing.Size(112, 23);
            this.butLoad.TabIndex = 11;
            this.butLoad.TabStop = false;
            this.butLoad.Text = "Load From File";
            this.butLoad.UseVisualStyleBackColor = true;
            this.butLoad.Click += new System.EventHandler(this.butLoad_Click);
            // 
            // saveFileDlg
            // 
            this.saveFileDlg.DefaultExt = "csv";
            this.saveFileDlg.Filter = "CSV File|*.csv";
            // 
            // openFileDlg
            // 
            this.openFileDlg.DefaultExt = "csv";
            this.openFileDlg.Filter = "CSV File|*.csv";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabGame);
            this.tabControl1.Controls.Add(this.tabSettings);
            this.tabControl1.Location = new System.Drawing.Point(12, 76);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(587, 452);
            this.tabControl1.TabIndex = 13;
            // 
            // tabGame
            // 
            this.tabGame.Controls.Add(this.pictureBox1);
            this.tabGame.Location = new System.Drawing.Point(4, 22);
            this.tabGame.Name = "tabGame";
            this.tabGame.Padding = new System.Windows.Forms.Padding(3);
            this.tabGame.Size = new System.Drawing.Size(579, 426);
            this.tabGame.TabIndex = 0;
            this.tabGame.Text = "game";
            this.tabGame.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Location = new System.Drawing.Point(21, 19);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(539, 388);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // tabSettings
            // 
            this.tabSettings.Controls.Add(this.groupBox2);
            this.tabSettings.Controls.Add(this.groupBox1);
            this.tabSettings.Location = new System.Drawing.Point(4, 22);
            this.tabSettings.Name = "tabSettings";
            this.tabSettings.Padding = new System.Windows.Forms.Padding(3);
            this.tabSettings.Size = new System.Drawing.Size(579, 426);
            this.tabSettings.TabIndex = 1;
            this.tabSettings.Text = "settings";
            this.tabSettings.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtQLearningItMaxMoves);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtQLearningIterations);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(6, 168);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(271, 101);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Q-Learning";
            // 
            // txtQLearningItMaxMoves
            // 
            this.txtQLearningItMaxMoves.Location = new System.Drawing.Point(141, 59);
            this.txtQLearningItMaxMoves.Maximum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.txtQLearningItMaxMoves.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.txtQLearningItMaxMoves.Name = "txtQLearningItMaxMoves";
            this.txtQLearningItMaxMoves.Size = new System.Drawing.Size(67, 20);
            this.txtQLearningItMaxMoves.TabIndex = 7;
            this.txtQLearningItMaxMoves.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtQLearningItMaxMoves.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.txtQLearningItMaxMoves.ValueChanged += new System.EventHandler(this.txtQLearningItMaxMoves_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 59);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Iteration Max Moves:";
            // 
            // txtQLearningIterations
            // 
            this.txtQLearningIterations.Location = new System.Drawing.Point(141, 27);
            this.txtQLearningIterations.Maximum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.txtQLearningIterations.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtQLearningIterations.Name = "txtQLearningIterations";
            this.txtQLearningIterations.Size = new System.Drawing.Size(67, 20);
            this.txtQLearningIterations.TabIndex = 5;
            this.txtQLearningIterations.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtQLearningIterations.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.txtQLearningIterations.ValueChanged += new System.EventHandler(this.txtQLearningIterations_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 29);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(129, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Training Iterations (x1000)";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.trackGameSpeed);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtBoadHeight);
            this.groupBox1.Controls.Add(this.txtBoadWidth);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(271, 156);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Game Settings";
            // 
            // trackGameSpeed
            // 
            this.trackGameSpeed.Location = new System.Drawing.Point(12, 90);
            this.trackGameSpeed.Minimum = 1;
            this.trackGameSpeed.Name = "trackGameSpeed";
            this.trackGameSpeed.Size = new System.Drawing.Size(248, 45);
            this.trackGameSpeed.TabIndex = 6;
            this.trackGameSpeed.TabStop = false;
            this.trackGameSpeed.Value = 6;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(9, 74);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Game Speed";
            // 
            // txtBoadHeight
            // 
            this.txtBoadHeight.Location = new System.Drawing.Point(84, 45);
            this.txtBoadHeight.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.txtBoadHeight.Name = "txtBoadHeight";
            this.txtBoadHeight.Size = new System.Drawing.Size(74, 20);
            this.txtBoadHeight.TabIndex = 4;
            this.txtBoadHeight.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBoadHeight.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.txtBoadHeight.ValueChanged += new System.EventHandler(this.txtBoadHeight_ValueChanged);
            // 
            // txtBoadWidth
            // 
            this.txtBoadWidth.Location = new System.Drawing.Point(84, 21);
            this.txtBoadWidth.Minimum = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.txtBoadWidth.Name = "txtBoadWidth";
            this.txtBoadWidth.Size = new System.Drawing.Size(74, 20);
            this.txtBoadWidth.TabIndex = 3;
            this.txtBoadWidth.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtBoadWidth.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.txtBoadWidth.ValueChanged += new System.EventHandler(this.txtBoadWidth_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Board Size Y:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Board Size X:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 49);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Algorithm:";
            // 
            // ctrlAlgorithm
            // 
            this.ctrlAlgorithm.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ctrlAlgorithm.FormattingEnabled = true;
            this.ctrlAlgorithm.Items.AddRange(new object[] {
            "Q-Learning",
            "Genetic Algorithm"});
            this.ctrlAlgorithm.Location = new System.Drawing.Point(70, 46);
            this.ctrlAlgorithm.Name = "ctrlAlgorithm";
            this.ctrlAlgorithm.Size = new System.Drawing.Size(121, 21);
            this.ctrlAlgorithm.TabIndex = 15;
            this.ctrlAlgorithm.TabStop = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(793, 540);
            this.Controls.Add(this.ctrlAlgorithm);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.butLoad);
            this.Controls.Add(this.butSave);
            this.Controls.Add(this.txtLogs);
            this.Controls.Add(this.butStop);
            this.Controls.Add(this.butTrainAI);
            this.Controls.Add(this.butStartGame);
            this.KeyPreview = true;
            this.Name = "MainForm";
            this.Text = "Snake AI POC";
            this.tabControl1.ResumeLayout(false);
            this.tabGame.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.tabSettings.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtQLearningItMaxMoves)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtQLearningIterations)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackGameSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBoadHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtBoadWidth)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button butStartGame;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button butTrainAI;
        private System.Windows.Forms.Button butStop;
        private System.Windows.Forms.Label txtLogs;
        private System.Windows.Forms.Button butSave;
        private System.Windows.Forms.Button butLoad;
        private System.Windows.Forms.SaveFileDialog saveFileDlg;
        private System.Windows.Forms.OpenFileDialog openFileDlg;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabGame;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TabPage tabSettings;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown txtBoadWidth;
        private System.Windows.Forms.NumericUpDown txtBoadHeight;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown txtQLearningIterations;
        private System.Windows.Forms.NumericUpDown txtQLearningItMaxMoves;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox ctrlAlgorithm;
        private System.Windows.Forms.TrackBar trackGameSpeed;
        private System.Windows.Forms.Label label6;
    }
}

