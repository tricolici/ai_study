﻿namespace pocAiLib.Learning.Supervised
{
    interface ISupervisedLearning
    {
        double Run(double[] input, double[] output);
    }
}
