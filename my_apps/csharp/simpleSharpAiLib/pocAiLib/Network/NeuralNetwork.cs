﻿using pocAiLib.Activation;
using pocAiLib.Layers;
using System;
using System.Collections.Generic;
using System.Text;

namespace pocAiLib.Neural
{
    public class NeuralNetwork
    {
        public int InputsCount { get; private set; }

        public double[] Output { get; private set; }

        public int LayersCount { get; private set; }

        public NetworkLayer[] Layers { get; private set; }

        public NeuralNetwork(IActivationFunction function, int inputsCount, params int[] neuronsCount)
        {
            ValidateConstructorArgs(function, inputsCount, neuronsCount);

            int layersCount = neuronsCount.Length;
            this.InputsCount = inputsCount;
            this.LayersCount = layersCount;
            this.Layers = new NetworkLayer[this.LayersCount];

            // create each layer
            for (int i = 0; i < Layers.Length; i++)
            {
                Layers[i] = new NetworkLayer(
                    // neurons count in the layer
                    neuronsCount[i],
                    // inputs count of the layer
                    (i == 0) ? inputsCount : neuronsCount[i - 1],
                    // activation function of the layer
                    function
                    );
            }
        }

        public void Randomize()
        {
            foreach(var layer in Layers)
            {
                layer.Randomize();
            }
        }

        public double[] Compute(double[] input)
        {
            // local variable to avoid mutlithread conflicts
            double[] output = input;

            // compute each layer
            foreach (var layer in Layers)
            {
                output = layer.Compute(output);
            }

            // assign output property as well (works correctly for single threaded usage)
            this.Output = output;

            return output;
        }

        public void Save(string fileName)
        {
            throw new NotImplementedException();
        }

        public void Load(string fileName)
        {
            throw new NotImplementedException();
        }

        private void ValidateConstructorArgs(IActivationFunction function, int inputsCount, int[] neuronsCount)
        {
            if (function == null)
                throw new ArgumentException("Activation function was not set");

            if (inputsCount < 1)
                throw new ArgumentException("Inputs count should be greater than 0");

            if (neuronsCount.Length == 0)
                throw new ArgumentException("Network should have at least one output layer.");

            for (int i = 0; i < neuronsCount.Length; i++)
                if (neuronsCount[i] < 1)
                    throw new ArgumentException(string.Format("Layer {0} should have at least 1 neuron", i + 1));
        }
    }
}
