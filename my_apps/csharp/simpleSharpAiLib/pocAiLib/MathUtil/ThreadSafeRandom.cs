﻿using System;
using System.Collections.Generic;
using System.Text;

namespace pocAiLib.MathUtil
{
    public static class ThreadSafeRandom
    {
        private static readonly object rndLock = new object();
        private static readonly Random rnd = new Random();

        public static double NextDouble()
        {
            lock(rndLock)
            {
                return rnd.NextDouble();
            }
        }
    }
}
