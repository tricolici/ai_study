﻿using pocAiLib.Activation;
using pocAiLib.MathUtil;
using System;

namespace pocAiLib.Neurons
{
    public class ActivationNeuron : AbstractNeuron
    {
        public double Threshold { get; set; } = 0;

        public IActivationFunction ActivationFunction { get; private set; }

        public ActivationNeuron(int inputs, IActivationFunction function) : base(inputs)
        {
            this.ActivationFunction = function;
        }

        public override void Randomize()
        {
            base.Randomize();
            Threshold = ThreadSafeRandom.NextDouble();
        }

        public override double Compute(double[] input)
        {
            if (input.Length != InputsCount)
                throw new ArgumentException("Wrong length of the input vector.");

            // initial sum value
            double sum = 0.0;

            // compute weighted sum of inputs
            for (int i = 0; i < Weights.Length; i++)
            {
                sum += Weights[i] * input[i];
            }
            sum += Threshold;

            // local variable to avoid mutlithreaded conflicts
            double output = ActivationFunction.Function(sum);
            // assign output property as well (works correctly for single threaded usage)
            this.Output = output;

            return output;
        }
    }
}
