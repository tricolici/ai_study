﻿using pocAiLib.MathUtil;

namespace pocAiLib.Neurons
{
    public abstract class AbstractNeuron
    {
        public int InputsCount { get; protected set; }

        public double Output { get; protected set; }

        public double[] Weights { get; protected set; }

        protected AbstractNeuron(int inputs)
        {
            InputsCount = inputs;
            Weights = new double[InputsCount];
        }

        protected AbstractNeuron(double[] weights)
        {
            InputsCount = weights.Length;
            Weights = weights.Clone() as double[];
        }

        public virtual void Randomize()
        {
            for (int i = 0; i < InputsCount; i++)
                Weights[i] = ThreadSafeRandom.NextDouble();
        }

        public abstract double Compute(double[] input);
    }
}
