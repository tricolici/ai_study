﻿using pocAiLib.MathUtil;

namespace pocAiLib.Activation
{
    public class ActivationSigmoid : IActivationFunction
    {
        public double Alpha { get; set; }

        public ActivationSigmoid(double alpha = 1.0)
        {
            Alpha = alpha;
        }

        public double Function(double x)
        {
            return BoundNumbers.Bound(1.0 / (1.0 + BoundMath.Exp(-Alpha * x)));
        }

        public double Derivative(double x)
        {
            double y = Function(x);
            return Derivative2(y);
        }

        public double Derivative2(double y)
        {
            return BoundNumbers.Bound(Alpha * y * (1 - y));
        }
    }
}
