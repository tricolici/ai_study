﻿using System;
using pocAiLib.MathUtil;

namespace pocAiLib.Activation
{
    public class ActivationTanh : IActivationFunction
    {
        public double Function(double x)
        {
            return (BoundMath.Exp(2 * x) - 1) / (BoundMath.Exp(2 * x) + 1);
        }

        public double Derivative(double x)
        {
            throw new NotImplementedException();
        }

        public double Derivative2(double y)
        {
            throw new NotImplementedException();
        }
    }
}
