﻿using System;
using pocAiLib.MathUtil;

namespace pocAiLib.Activation
{
    public class ActivationGaussian : IActivationFunction
    {
        public double Function(double x)
        {
            return BoundMath.Exp(-BoundMath.Pow(2.5 * x, 2.0));
        }

        public double Derivative(double x)
        {
            double y = Function(x);
            return Derivative2(y);
        }

        public double Derivative2(double y)
        {
            return BoundMath.Exp(BoundMath.Pow(2.5 * y, 2.0) * 12.5 * y);
        }
    }
}
