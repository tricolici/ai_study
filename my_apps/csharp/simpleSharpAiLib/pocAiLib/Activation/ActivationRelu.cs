﻿using System;

namespace pocAiLib.Activation
{
    public class ActivationRelu : IActivationFunction
    {
        public double Function(double x)
        {
            return x > 0 ? x : 0;
        }

        public double Derivative(double x)
        {
            double y = Function(x);
            throw new NotImplementedException();
        }

        public double Derivative2(double y)
        {
            throw new NotImplementedException();
        }        
    }
}
