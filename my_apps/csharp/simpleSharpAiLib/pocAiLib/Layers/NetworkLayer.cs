﻿using pocAiLib.Activation;
using pocAiLib.Neurons;
using System;

namespace pocAiLib.Layers
{
    public class NetworkLayer
    {
        public int InputsCount { get; private set; }
        public double[] Output { get; private set; }

        public int NeuronsCount { get; private set; }

        public ActivationNeuron[] Neurons { get; private set; }

        public NetworkLayer(int neuronsCount, int inputsCount, IActivationFunction function)
        {
            InputsCount = Math.Max(1, inputsCount);
            NeuronsCount = Math.Max(1, neuronsCount);

            Neurons = new ActivationNeuron[NeuronsCount];
            for (int i = 0; i < NeuronsCount; i++)
            {
                Neurons[i] = new ActivationNeuron(inputsCount, function);
            }
        }

        public double[] Compute(double[] input)
        {
            // local variable to avoid mutlithread conflicts
            double[] output = new double[NeuronsCount];

            // compute each neuron
            for (int i = 0; i < Neurons.Length; i++)
                output[i] = Neurons[i].Compute(input);

            // assign output property as well (works correctly for single threaded usage)
            this.Output = output;

            return output;
        }

        public void Randomize()
        {
            foreach (ActivationNeuron neuron in Neurons)
                neuron.Randomize();
        }
    }
}
