﻿using pocAiLib.Activation;
using pocAiLib.Learning.Supervised;
using pocAiLib.Neural;
using System;
using System.Collections.Generic;
using System.Text;

namespace sample1.samples
{
    public class SampleNeuralNetworkXor
    {

        private readonly List<double[]> trainingInput = new List<double[]>
        {
            new double[] { 0, 0},
            new double[] { 0, 1},
            new double[] { 1, 0},
            new double[] { 1, 1}
        };

        private readonly List<double[]> trainingDesiredOutput = new List<double[]>
        {
            new double[]{ 0 },
            new double[]{ 1 },
            new double[]{ 1 },
            new double[]{ 0 }
        };

        NeuralNetwork network = null;

        private void CreateNeuralNetwork()
        {
            // Create Neural Network with:
            // 2 - input neurons
            // 8 - hidden neurons
            // 1 - output neuron
            network = new NeuralNetwork(new ActivationSigmoid(), 2, 10, 1);
            network.Randomize();
        }

        private void TestNeuralNetwork()
        {
            for (int i = 0; i < trainingInput.Count; i++)
            {
                PrintArray("INPUT", trainingInput[i]);

                double[] output = network.Compute(trainingInput[i]);

                PrintArray(" DESIRED", trainingDesiredOutput[i]);
                PrintArray(" OUTPUT", output);
                Console.WriteLine();
            }
        }
        
        
        public void Execute()
        {
            CreateNeuralNetwork();

            TestNeuralNetwork();

            Console.WriteLine("Starting Neural Network training");

            var teacher = new ResilientBackpropagationLearning(network);

            int it = 1;
            while (true)
            {
                double error = teacher.RunEpoch(trainingInput, trainingDesiredOutput);
                Console.WriteLine(string.Format("Iteration {0}. Error: {1:0.0000}", it, error));
                it++;
                if (error < 0.001)
                    break;
            }

            Console.WriteLine("\nTraining finished. Lets test it!");

            // test how NN was trained
            TestNeuralNetwork();
        }

        private void PrintArray(string title, double[] arr)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < arr.Length; i++)
            {
                sb.Append(arr[i]);
                if (i != arr.Length - 1)
                {
                    sb.Append(", ");
                }
            }
            Console.Write(String.Format("{0} is = [{1}]", title, sb.ToString()));
        }
    }
}
