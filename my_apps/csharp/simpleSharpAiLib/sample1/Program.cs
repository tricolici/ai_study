﻿using System;
using sample1.samples;

namespace sample1
{
    class Program
    {
        static void Main(string[] args)
        {
            var sampleXor = new SampleNeuralNetworkXor();
            sampleXor.Execute();
        }
    }
}
