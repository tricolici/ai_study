import numpy as np

# =============================================================
def get_poss_next_states(s, map, ns):
    poss_next_states = []
    for j in range(ns):
        if map[s, j] == 1: poss_next_states.append(j)
    return poss_next_states
# =============================================================
def get_rnd_next_state(s, map, ns):
    poss_next_states = get_poss_next_states(s, map, ns)
    next_state = \
        poss_next_states[np.random.randint(0, len(poss_next_states))]
    return next_state
# =============================================================
def train_iteration(map, rewards, quality, gamma, lrn_rate, goal, ns):
    curr_s = np.random.randint(0, ns)  # random start state

    while (True):
        next_s = get_rnd_next_state(curr_s, map, ns)
        poss_next_next_states = \
            get_poss_next_states(next_s, map, ns)

        max_Q = -9999.99
        for j in range(len(poss_next_next_states)):
            nn_s = poss_next_next_states[j]
            q = quality[next_s, nn_s]
            if q > max_Q:
                max_Q = q
        # Q = [(1-a) * Q]  +  [a * (rt + (g * maxQ))]
        quality[curr_s][next_s] = ((1 - lrn_rate) * quality[curr_s] \
            [next_s]) + (lrn_rate * (rewards[curr_s][next_s] + \
                                     (gamma * max_Q)))

        curr_s = next_s
        if curr_s == goal: break
# =============================================================
def walk(start, goal, quality):
    print("Using Q to go from {s} to goal {g}".format(s=start, g=goal))
    curr = start
    print(str(curr) + "->", end="")
    printed_count=0
    while curr != goal:
        next = np.argmax(quality[curr])
        print(str(next) + "->", end="")
        curr = next
        printed_count+=1
        if (printed_count > 15):
            break
    print("done")
# =============================================================
def main():
    print("Setting up maze in memory")
    # define MAP !
    map = np.zeros(shape=[15, 15], dtype=int)  # Feasible
    map[0, 1] = 1; map[0, 5] = 1; map[1, 0] = 1; map[2, 3] = 1; map[3, 2] = 1; map[3, 4] = 1; map[3, 8] = 1;
    map[4, 3] = 1; map[4, 9] = 1; map[5, 0] = 1; map[5, 6] = 1; map[5, 10] = 1; map[6, 5] = 1; map[7, 8] = 1; map[7, 12] = 1;
    map[8, 3] = 1; map[8, 7] = 1; map[9, 4] = 1; map[9, 14] = 1; map[10, 5] = 1; map[10, 11] = 1; map[11, 10] = 1; map[11, 12] = 1;
    map[12, 7] = 1; map[12, 11] = 1; map[12, 13] = 1; map[13, 12] = 1; map[14, 14] = 1
    # define rewards for each possible move
    rewards = np.zeros(shape=[15, 15], dtype=int)
    rewards[9,14] = 10
    # quality
    quality = np.zeros(shape=[15, 15], dtype=np.float32)
    print("Analyzing maze with RL Q-learning")
    start = 0; goal = 14
    ns = 15  # number of states
    gamma = 0.5 # discount factor, influences the importance of future rewards
    lrn_rate = 0.5
    max_epochs = 10
    for i in range(0, max_epochs):
        print("training iteration " + str(i+1))
        train_iteration(map, rewards, quality, gamma, lrn_rate, goal, ns)
    print("Done ")

    walk(0, 14, quality)
# =============================================================

if __name__ == "__main__":
  main()