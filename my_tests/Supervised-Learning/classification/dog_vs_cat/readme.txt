Download dataset from here: https://storage.googleapis.com/mledu-datasets/cats_and_dogs_filtered.zip

unzip to 'dataset' directory:

dataset/
  cats_and_dogs_filtered/
    train/
        cats/
            cat.0.jpg
            cat.1.jpg
            ...
        dogs/
            dog.0.jpg
    validation/
        cats/
        dogs/

