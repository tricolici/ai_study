from dataset import MyDataset
from neural_network import NeuralNetwork

import os, psutil

def show_memusage():
    process = psutil.Process(os.getpid())
    print(process.memory_info().rss)  # in bytes

if __name__ == "__main__" :
    data=MyDataset()
    print("Loading training data")
    data.load_training_data()

    print("Loading validation data")
    data.load_test_data()
    print("Done")
