from torch import nn
import configuration as cfg

class NeuralNetwork(nn.Module):
    def __init__(self):
        super().__init__()
        self.layer1=nn.Linear(cfg.NN_INPUT_NEURONS, cfg.NN_HIDDEN1_NEURONS)
        self.layer2=nn.Linear(cfg.NN_HIDDEN1_NEURONS, cfg.NN_HIDDEN2_NEURONS)
        self.layer3=nn.Linear(cfg.NN_HIDDEN2_NEURONS, cfg.NN_OUTPUT_NEURONS)
        self.sigmoid = nn.Sigmoid()
        self.softmax = nn.Softmax(dim=1)

    def forward(self, x):
        x = self.sigmoid(self.layer1(x))
        x = self.sigmoid(self.layer2(x))
        x = self.softmax(self.layer3(x))
        return x