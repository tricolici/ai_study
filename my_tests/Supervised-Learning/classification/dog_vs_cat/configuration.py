
IMAGE_WIDTH=100
IMAGE_HEIGHT=100

TRAINING_CATS_PATH="dataset/cats_and_dogs_filtered/train/cats"
TRAINING_DOGS_PATH="dataset/cats_and_dogs_filtered/train/dogs"

VALIDATION_CATS_PATH="dataset/cats_and_dogs_filtered/validation/cats"
VALIDATION_DOGS_PATH="dataset/cats_and_dogs_filtered/validation/dogs"

NN_INPUT_NEURONS=IMAGE_WIDTH*IMAGE_HEIGHT
NN_HIDDEN1_NEURONS=100
NN_HIDDEN2_NEURONS=80
NN_OUTPUT_NEURONS=2 # one neuron for cat and other for dog