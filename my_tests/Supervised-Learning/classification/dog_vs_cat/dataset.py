import os
import numpy as np
from PIL import Image
import configuration as cfg

class MyDataset:
    def __init__(self):
        self.cats=[]
        self.dogs=[]

    def clear(self):
        self.cats.clear()
        self.dogs.clear()

    def load_training_data(self):
        self.clear()
        self.load_images(cfg.TRAINING_CATS_PATH, self.cats, 1)
        self.load_images(cfg.TRAINING_DOGS_PATH, self.dogs, 2)

    def load_test_data(self):
        self.clear()
        self.load_images(cfg.VALIDATION_CATS_PATH, self.cats, 1)
        self.load_images(cfg.VALIDATION_DOGS_PATH, self.dogs, 2)

    def load_images(self, path, target_list, cat_or_dog):
        for imgName in os.listdir(path):
            with Image.open(os.path.join(path, imgName)).resize((cfg.IMAGE_WIDTH,cfg.IMAGE_HEIGHT)).convert('L') as img:
                data=np.asarray(img).flatten()
                target_list.append((data, cat_or_dog))

